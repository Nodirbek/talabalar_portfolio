-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 06 2018 г., 13:08
-- Версия сервера: 5.5.57
-- Версия PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `talaba_reyting`
--

-- --------------------------------------------------------

--
-- Структура таблицы `anketa`
--

CREATE TABLE `anketa` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `familiya` varchar(50) NOT NULL,
  `ism` varchar(50) NOT NULL,
  `sharif` varchar(50) NOT NULL,
  `millat` varchar(20) NOT NULL,
  `pass_ser` varchar(20) NOT NULL,
  `pass_raq` int(11) NOT NULL,
  `tugatgan_k` varchar(255) DEFAULT NULL,
  `tugatgan_yili` varchar(255) DEFAULT NULL,
  `mutaxasislik` varchar(255) DEFAULT NULL,
  `staj` varchar(255) DEFAULT NULL,
  `sport` varchar(255) DEFAULT NULL,
  `kirish_ball` float DEFAULT NULL,
  `kirish_turi` varchar(20) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `tug_sana` date NOT NULL,
  `jinsi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `anketa`
--

INSERT INTO `anketa` (`id`, `user_id`, `familiya`, `ism`, `sharif`, `millat`, `pass_ser`, `pass_raq`, `tugatgan_k`, `tugatgan_yili`, `mutaxasislik`, `staj`, `sport`, `kirish_ball`, `kirish_turi`, `avatar`, `tug_sana`, `jinsi`) VALUES
(14, 39, 'Matchanov', 'Nodirbek', 'Sherzodovich', 'uzbek', 'AA', 545454, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1541433415.jpg', '1996-01-05', 'erkak'),
(15, 40, 'test', 'test', 'twat', 'uzbek', 'AA', 5555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', '2018-11-11', 'erkak');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1541135604),
('m130524_201442_init', 1541136818);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(39, 'Nodirbek', 'acNmiHxiaibva2Aniw0JF2F557lmkymS', '$2y$13$ndsBwJAkjR3lISlH3Rf7pOoxKADQHRcq6zK4/Y1slDFSQqJDyvBLG', NULL, 'nodir2@mail.ru', 10, 1541433415, 1541433415),
(40, 'test', 'YJi6OjgDsNNaEycpdGwFfrLO_V9Nr3H6', '$2y$13$7xUUkcLTzrmLp8D1d2JRuu3O3KAvXyWtscGAUjYCeQxVFYSwYl1aW', NULL, 'group.scala@mail.ru', 10, 1541491306, 1541498681);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `anketa`
--
ALTER TABLE `anketa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `anketa`
--
ALTER TABLE `anketa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `anketa`
--
ALTER TABLE `anketa`
  ADD CONSTRAINT `anketa_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
