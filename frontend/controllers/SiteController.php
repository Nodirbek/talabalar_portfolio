<?php
namespace frontend\controllers;

use app\models\Anketa;
use frontend\models\Faoliyatlar;
use frontend\models\FaoliyatlarTuri;
use frontend\models\FaoliyatMavzu;
use frontend\models\TalabalarSoravlari;
use PHPUnit\Framework\Exception;
use Yii;
use yii\base\InvalidParamException;
use yii\debug\models\search\User;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionKabinet()
    {
        if (!Yii::$app->user->isGuest) {
        $model = new Anketa();
        $user_id = Yii::$app->user->getId();
        $stat = new TalabalarSoravlari();
        $ball = $stat->getStudentBall($user_id);
        if ($model->load(Yii::$app->request->post())) {
            $name = time();

            $upload = UploadedFile::getInstance($model, 'avatar');
            if ($upload != null && $upload->saveAs('../web/avatar/' . $name . '.' . $upload->extension)) {
                $fayl = $name . '.' . $upload->extension;
                $anketa = Anketa::find()->where(['user_id' => Yii::$app->user->identity->getId()])->one();
                if (file_exists(('../web/avatar/' . $anketa->avatar))) {
//                    Yii::$app->session->setFlash('error', 'Rasm o`chirildi.');
                    unlink('../web/avatar/' . $anketa->avatar);
                }
                $anketa->avatar = $fayl;
                $anketa->update();
                Yii::$app->session->setFlash('success', 'Rasm yangilandi.');

            }

        }
        return $this->render('kabinet', [
            'model' => $model,
            'ball' => $ball
        ]);
    }
    else{
            $this->redirect('/site/login');
    }
    }
    public function actionInfo()
    {
       
        $user_id = Yii::$app->request->get('id_user');
        $user_anketa = \app\models\Anketa::find()->where(['id' => $user_id])->one();
        $user = \common\models\User::find()->where(['id' => $user_anketa->user_id])->one();
        $stat = new TalabalarSoravlari();
        $ball = $stat->getStudentBall($user->id);
        
        return $this->render('info', [
            'user_anketa' => $user_anketa,
            'ball' => $ball,
            'user' => $user
        ]);
    }

    public function actionScientific()
    {
        $user_id = Yii::$app->user->identity->getId();
        $stat = new TalabalarSoravlari();
        $ball = $stat->getStudentBall($user_id);

        return $this->render('scientific', [
            'ball' => $ball
        ]);
    }

    public function actionManaviy()
    {
        return $this->render('reyting');
    }

    public function actionUpload()
    {
        if (Yii::$app->request->get('id')) {
            $id = Yii::$app->request->get('id');
            $data = \frontend\models\Faoliyatlar::find()->where(['nomi' => $id])->all();
            $uploaded_files = \frontend\models\TalabalarSoravlari::find()->select('talabalar_soravlari.id,faoliyatlar.nomi as nomi,fayl,faoliyatlar.izoh as type,tasdiqlash')
                ->from(\frontend\models\TalabalarSoravlari::tableName())
                ->where(['talaba_id' => Yii::$app->user->getId()])->
                innerJoin(\frontend\models\Faoliyatlar::tableName(), 'faoliyatlar.id = talabalar_soravlari.turi_id and faoliyatlar.nomi = ' . $id)
                ->asArray()->all();
        }
        $user_id = Yii::$app->user->identity->getId();
        $stat = new TalabalarSoravlari();
        $ball = $stat->getStudentBall($user_id);
        $model = new TalabalarSoravlari();
        if (Yii::$app->request->post()) {
            $model->talaba_id = Yii::$app->user->identity->getId();
            $model->izoh = $_POST['TalabalarSoravlari']['izoh'];
            $model->turi_id = $_POST['id'];
            $model->tasdiqlash = 0;
            $model->save();
            Yii::$app->session->setFlash('success', 'Fayl yuklandi');

        }
        return $this->render('upload', [
            'model' => $model,
            'ball' => $ball,
            'data' => $data,
            'uploaded_files' => $uploaded_files,
            'id' => $id,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }


        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/site/kabinet');
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Sizning emailingizga parolni qayta tiklash so`rovi jo`natildi iltimos tekshirib ko`ring!  ');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Kechirasiz, sizning emailingizga parolni qayta tiklash uchun so`rov jo`nata olmadik.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionReset()
    {
        try {
            $send = Yii::$app->mailer->compose()
                ->setFrom('group.scala@mail.ru')
                ->setTo('group.scala@mail.ru')
                ->setSubject('test')
                ->setTextBody('test')
                ->setHtmlBody('<p>Ismi:</p>' . '<p>Email: ' . '</p><h5><b> Xabar mazmuni: </b></h5><h5>' . '</h5>');

            $send->send();
        } catch (Exception $e) {
            var_dump($e);
        }
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Yangi parol saqlandi.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    // ChangePssword account
    public function actionChangePassword()
    {
        if (Yii::$app->request->post('newPasswd')) {
            $user = \app\models\User::find()->where(['id' => Yii::$app->user->identity->getId()])->one();

            $newPasswd = Yii::$app->request->post('newPasswd');
            $currentPasswd = Yii::$app->request->post('currentPasswd');
//            echo "<pre>";
//            print_r();
//            die();
            if (Yii::$app->security->validatePassword($currentPasswd, $user->password_hash)) {
//                $user = \app\models\User::find();
                $user->password_hash = Yii::$app->security->generatePasswordHash($newPasswd);
                Yii::$app->session->setFlash('success', 'Yangi parol saqlandi.');
                $user->update();
            } else {
                Yii::$app->session->setFlash('error', 'Parol xato terildi');
            }

        }
        return $this->render('change-password');
    }

    // ChangePssword account
    public function actionReyting()
    {
        $user_id = Yii::$app->user->identity->getId();
        $stat = new TalabalarSoravlari();
        $ball = $stat->getStudentBall($user_id);
        $model = FaoliyatlarTuri::find()->select('faoliyatlar_turi.nomi as f_t_nomi,SUM(ball) as ball')
                ->innerJoin(FaoliyatMavzu::tableName(), 'faoliyat_mavzu.id_tur = faoliyatlar_turi.id')
            ->innerJoin(Faoliyatlar::tableName(), 'faoliyatlar.nomi = faoliyat_mavzu.id')
            ->innerJoin(TalabalarSoravlari::tableName(),'talabalar_soravlari.turi_id = faoliyatlar.id')
            ->where('talabalar_soravlari.talaba_id = '.Yii::$app->user->identity->getId().' and tasdiqlash = 1')
            ->groupBy('faoliyatlar_turi.id')->asArray()
            ->all();
         return $this->render('reyting',[
             'model'=>$model,
             'ball'=>$ball
         ]);
    }

    // Edit data account
    public function actionDelete()
    {
        if (Yii::$app->request->get()) {
            $id = Yii::$app->request->get('id');
            $file = Yii::$app->request->get('file');
            $tur_id = Yii::$app->request->get('tur_id');
            if (file_exists(('../web/file/' . $file)))
                unlink('../web/file/' . $file);
            $model = TalabalarSoravlari::find()->where(['id' => $id])->one();
            $model->delete();
        }
        return $this->redirect('/site/upload?id=' . $tur_id);
    }
}
