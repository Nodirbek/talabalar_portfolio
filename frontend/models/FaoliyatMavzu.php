<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "faoliyat_mavzu".
 *
 * @property int $id
 * @property string $nomi
 * @property int $id_tur
 */
class FaoliyatMavzu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faoliyat_mavzu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nomi', 'id_tur'], 'required'],
            [['id_tur'], 'integer'],
            [['nomi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomi' => 'Nomi',
            'id_tur' => 'Id Tur',
        ];
    }
}
