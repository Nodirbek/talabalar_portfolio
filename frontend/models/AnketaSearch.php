<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Anketa;

/**
 * AnketaSearch represents the model behind the search form of `app\models\Anketa`.
 */
class AnketaSearch extends Anketa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'pass_raq'], 'integer'],
            [['familiya', 'ism', 'sharif', 'millat', 'pass_ser', 'tugatgan_k', 'tugatgan_yili', 'mutaxasislik', 'staj', 'sport', 'kirish_turi', 'avatar', 'tug_sana', 'jinsi'], 'safe'],
            [['kirish_ball'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Anketa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'pass_raq' => $this->pass_raq,
            'kirish_ball' => $this->kirish_ball,
            'tug_sana' => $this->tug_sana,
        ]);

        $query->andFilterWhere(['like', 'familiya', $this->familiya])
            ->andFilterWhere(['like', 'ism', $this->ism])
            ->andFilterWhere(['like', 'sharif', $this->sharif])
            ->andFilterWhere(['like', 'millat', $this->millat])
            ->andFilterWhere(['like', 'pass_ser', $this->pass_ser])
            ->andFilterWhere(['like', 'tugatgan_k', $this->tugatgan_k])
            ->andFilterWhere(['like', 'tugatgan_yili', $this->tugatgan_yili])
            ->andFilterWhere(['like', 'mutaxasislik', $this->mutaxasislik])
            ->andFilterWhere(['like', 'staj', $this->staj])
            ->andFilterWhere(['like', 'sport', $this->sport])
            ->andFilterWhere(['like', 'kirish_turi', $this->kirish_turi])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'jinsi', $this->jinsi]);

        return $dataProvider;
    }
}
