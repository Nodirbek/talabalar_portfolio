<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "faoliyatlar".
 *
 * @property int $id
 * @property int $nomi
 * @property string $izoh
 * @property int $ball
 *
 * @property TalabalarSoravlari[] $talabalarSoravlaris
 */
class Faoliyatlar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faoliyatlar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nomi', 'izoh', 'ball'], 'required'],
            [['nomi', 'ball'], 'integer'],
            [['izoh'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomi' => 'Nomi',
            'izoh' => 'Izoh',
            'ball' => 'Ball',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTalabalarSoravlaris()
    {
        return $this->hasMany(TalabalarSoravlari::className(), ['turi_id' => 'id']);
    }
}
