<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "anketa".
 *
 * @property int $id
 * @property int $user_id
 * @property string $familiya
 * @property string $ism
 * @property string $sharif
 * @property string $millat
 * @property string $pass_ser
 * @property int $pass_raq
 * @property string $tugatgan_k
 * @property string $tugatgan_yili
 * @property string $mutaxasislik
 * @property string $staj
 * @property string $sport
 * @property double $kirish_ball
 * @property string $kirish_turi
 * @property string $avatar
 * @property string $tug_sana
 * @property string $jinsi
 *
 * @property User $user
 */
class Anketa extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'anketa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'familiya', 'ism', 'sharif', 'millat', 'pass_ser', 'pass_raq', 'tug_sana', 'jinsi'], 'required'],
            [['user_id', 'pass_raq'], 'integer'],
            [['kirish_ball'], 'number'],
            [['tug_sana'], 'safe'],
            [['familiya', 'ism', 'sharif'], 'string', 'max' => 50],
            [['millat', 'pass_ser', 'kirish_turi', 'jinsi'], 'string', 'max' => 20],
            [['tugatgan_k', 'tugatgan_yili', 'mutaxasislik', 'staj', 'sport', 'avatar'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */

    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'familiya' => 'Familiya',
            'ism' => 'Ism',
            'sharif' => 'Sharif',
            'millat' => 'Millat',
            'pass_ser' => 'Pass Ser',
            'pass_raq' => 'Pass Raq',
            'tugatgan_k' => 'Tugatgan K',
            'tugatgan_yili' => 'Tugatgan Yili',
            'mutaxasislik' => 'Mutaxasislik',
            'staj' => 'Staj',
            'sport' => 'Sport',
            'kirish_ball' => 'Kirish Ball',
            'kirish_turi' => 'Kirish Turi',
            'avatar' => '',
            'tug_sana' => 'Tug Sana',
            'jinsi' => 'Jinsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
