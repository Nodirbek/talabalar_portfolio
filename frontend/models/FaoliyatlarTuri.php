<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "faoliyatlar_turi".
 *
 * @property int $id
 * @property string $nomi
 *
 * @property Faoliyatlar[] $faoliyatlars
 */
class FaoliyatlarTuri extends \yii\db\ActiveRecord
{
    public $f_t_nomi;
    public $izoh;
    public $ball;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faoliyatlar_turi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nomi'], 'required'],
            [['nomi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomi' => 'Nomi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaoliyatlars()
    {
        return $this->hasMany(Faoliyatlar::className(), ['turi_id' => 'id']);
    }
}
