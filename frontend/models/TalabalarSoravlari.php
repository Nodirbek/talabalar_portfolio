<?php

namespace frontend\models;

use app\models\Anketa;
use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "talabalar_soravlari".
 *
 * @property int $id
 * @property int $turi_id
 * @property string $izoh
 * @property string $fayl
 * @property int $talaba_id
 * @property int $tasdiqlash
 *
 * @property Faoliyatlar $turi
 */
class TalabalarSoravlari extends \yii\db\ActiveRecord
{
    public $upload;
    public $ball;
//    public $filepath;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'turi_id' => 'Turi ID',
            'izoh' => 'Izoh',
            'fayl' => 'Fayl',
            'upload' => '',
            'talaba_id' => 'Talaba ID',
            'tasdiqlash' => 'Tasdiqlash',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTuri()
    {
        return $this->hasOne(Faoliyatlar::className(), ['id' => 'turi_id']);
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'talabalar_soravlari';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turi_id'], 'required'],
            [['turi_id', 'talaba_id', 'tasdiqlash'], 'integer'],
            [['izoh', 'fayl'], 'string', 'max' => 255],
//            [['upload'], 'file',  'extensions' => 'rar, zip'],
            [['turi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faoliyatlar::className(), 'targetAttribute' => ['turi_id' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {

        $name = time();

        $this->upload = UploadedFile::getInstance($this, 'upload');
        if ($this->upload != null && $this->upload->saveAs('../web/file/' . $name . '.' . $this->upload->extension)) {
            $this->fayl = $name . '.' . $this->upload->extension;
            $anketa = new TalabalarSoravlari();
            $anketa->fayl = $name . '.' . $this->upload->extension;
        }

        return true;

    }

    public function afterDelete()
    {
        if (file_exists(('../web/file/' . $this->fayl)))
            unlink('..web/file/' . $this->fayl);
        return true;
    }

    public function getStudentBall($id)
    {

        $uploaded_files = \frontend\models\TalabalarSoravlari::find()->select('SUM(faoliyatlar.ball) as ball')
            ->from(\frontend\models\TalabalarSoravlari::tableName())
            ->where(['talaba_id' => $id,'tasdiqlash' => 1])->
            innerJoin(\frontend\models\Faoliyatlar::tableName(), 'faoliyatlar.id = talabalar_soravlari.turi_id ')
            ->one();
        return $uploaded_files->ball;

    }
}
