<?php
namespace frontend\models;

use app\models\Anketa;
use PHPUnit\Framework\Exception;
use yii\base\Model;
use common\models\User;
use yii\web\UploadedFile;
use Yii;


/**
 * Signup form
 * * @property string $familiya
 * @property string $ism
 * @property string $sharif
 * @property string $millat
 * @property string $pass_ser
 * @property int $pass_raq
 * @property string $tugatgan_k
 * @property string $tugatgan_yili
 * @property string $mutaxasislik
 * @property string $staj
 * @property string $sport
 * @property double $kirish_ball
 * @property string $kirish_turi
 * @property string $avatar
 *
 * @property string $tug_sana
 * @property string $jinsi
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $ism;
    public $sharif;
    public $familiya;
    public $tug_sana;
    public $millat;
    public $pass_ser;
    public $pass_raq;
    public $tugatgan_k;
    public $tugatgan_yili;
    public $mutaxasislik;
    public $staj;
    public $sport;
    public $kirish_ball;
    public $kirish_turi;
    public $avatar;
    public $jinsi;
    public $imageFile;
    public $path;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'familiya', 'ism', 'sharif', 'millat', 'pass_ser', 'pass_raq', 'tug_sana', 'jinsi'], 'required'],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [['pass_raq'], 'integer'],
            [['kirish_ball'], 'number'],
            [['tug_sana'], 'safe'],
            [['familiya', 'ism', 'sharif'], 'string', 'max' => 50],
            [['millat', 'pass_ser', 'kirish_turi', 'jinsi'], 'string', 'max' => 20],
            [['tugatgan_k', 'tugatgan_yili', 'mutaxasislik', 'staj', 'sport', 'avatar'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tug_sana' => 'Tug`ilgan sanasi',
            'pass_raq' => 'Passport raqami',
            'username' => 'Login',
            'password' => 'Parol',
            'sharif' => 'Otasining ismi',
            'staj' => "Ish tajribasi(mavjud bo'lsa)",
            'sport' => "Shug'ulanadigan sport turi",
            'pass_ser' => 'Passport seriyasi',
            'millat' => 'Millati',
            'kirish_turi' => 'Kirish turi',
            'ism' => 'Ismi',
            'familiya' => 'Familiyasi',
            'tugatgan_k' => "Tugatgan ta'lim muassasasi",
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function beforeSave($insert){

        $name = time();

        $this->imageFile = UploadedFile::getInstance($this,'avatar');
        if($this->imageFile != null &&  $this->imageFile->saveAs(Yii::$app->request->baseUrl.'/avatar/' . $name . '.' . $this->imageFile->extension)){
            $anketa = new Anketa();
            $this->path = $name . '.' . $this->imageFile->extension;
            $anketa->avatar = $name . '.' . $this->imageFile->extension;
        }

        return true;

    }
    public function afterDelete(){
        if(file_exists(('img/' .$this->img)))
            unlink('img/' .$this->img);
        return true;
    }
    

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user_info = new Anketa();
        $user_info->familiya = $this->familiya;
        $user_info->ism = $this->ism;
        $user_info->sharif = $this->sharif;
        $user_info->jinsi = $this->jinsi;
        $user_info->sport = $this->sport;
        $user_info->pass_raq = $this->pass_raq;
        $user_info->pass_ser = $this->pass_ser;
        $user_info->tug_sana = $this->tug_sana;
        $user_info->tugatgan_k = $this->tugatgan_k;
        $user_info->kirish_ball = $this->kirish_ball;
        $user_info->kirish_turi = $this->kirish_turi;
        $user_info->millat = $this->millat;
        $user_info->mutaxasislik = $this->mutaxasislik;
        $name = time();
        $this->imageFile = UploadedFile::getInstance($this,'avatar');
        if($this->imageFile != null &&  $this->imageFile->saveAs('../web/avatar/' . $name . '.' . $this->imageFile->extension)){
            $anketa = new Anketa();
            $this->path = $name . '.' . $this->imageFile->extension;
            $user_info->avatar = $name . '.' . $this->imageFile->extension; 
        } else {
            $user_info->avatar = 'avatar.png';
        }
        $saved_user = $user->save();
        $user_update = new User();
        $last_id = $user_update->getDb()->lastInsertID;
        $userRole = Yii::$app->authManager->getRole('user');
        Yii::$app->authManager->assign($userRole, $last_id);
        $user_info->user_id = $last_id;

        $user_info->save();
        return $saved_user ? $user : null;
    }
}
