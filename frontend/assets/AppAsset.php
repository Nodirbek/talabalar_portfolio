<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/font-awesome.min.css',
        'css/hover-min.css',
        'css/jquery.bxslider.css',
//        'css/bulma.min.css',
//        'css/nexus.css',
//        'css/bootstrap.min.css',
    ];
    public $js = [
//        'js/jquery.min.js',
        'js/jquery.bxslider.min.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
       public $jsOptions = [ 'position' => \yii\web\View::POS_END ];

}
