-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 10 2018 г., 09:09
-- Версия сервера: 5.5.57
-- Версия PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `talaba_reyting`
--

-- --------------------------------------------------------

--
-- Структура таблицы `anketa`
--

CREATE TABLE `anketa` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `familiya` varchar(50) NOT NULL,
  `ism` varchar(50) NOT NULL,
  `sharif` varchar(50) NOT NULL,
  `millat` varchar(20) NOT NULL,
  `pass_ser` varchar(20) NOT NULL,
  `pass_raq` int(11) NOT NULL,
  `tugatgan_k` varchar(255) DEFAULT NULL,
  `tugatgan_yili` varchar(255) DEFAULT NULL,
  `mutaxasislik` varchar(255) DEFAULT NULL,
  `staj` varchar(255) DEFAULT NULL,
  `sport` varchar(255) DEFAULT NULL,
  `kirish_ball` float DEFAULT NULL,
  `kirish_turi` varchar(20) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `tug_sana` date NOT NULL,
  `jinsi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `anketa`
--

INSERT INTO `anketa` (`id`, `user_id`, `familiya`, `ism`, `sharif`, `millat`, `pass_ser`, `pass_raq`, `tugatgan_k`, `tugatgan_yili`, `mutaxasislik`, `staj`, `sport`, `kirish_ball`, `kirish_turi`, `avatar`, `tug_sana`, `jinsi`) VALUES
(14, 39, 'Matchanov', 'Nodirbek', 'Sherzodovich', 'uzbek', 'AA', 545454, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1541433415.jpg', '1996-01-05', 'erkak'),
(15, 40, 'test', 'test', 'twat', 'uzbek', 'AA', 5555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', '2018-11-11', 'erkak'),
(16, 41, 'Matchanov', 'Nodirbek', 'test', 'uzbek', 'AA', 2555, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'avatar.png', '2018-01-02', 'erkak'),
(17, 42, 'SDF', 'ASD', 'DSF', 'uzbek', 'AA', 333, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1541788413.jpg', '2018-01-01', 'erkak');

-- --------------------------------------------------------

--
-- Структура таблицы `faoliyatlar`
--

CREATE TABLE `faoliyatlar` (
  `id` int(11) NOT NULL,
  `turi_id` int(11) NOT NULL,
  `nomi` varchar(255) NOT NULL,
  `izoh` varchar(255) NOT NULL,
  `ball` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faoliyatlar`
--

INSERT INTO `faoliyatlar` (`id`, `turi_id`, `nomi`, `izoh`, `ball`) VALUES
(1, 1, 'as', 'AS', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `faoliyatlar_turi`
--

CREATE TABLE `faoliyatlar_turi` (
  `id` int(11) NOT NULL,
  `nomi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faoliyatlar_turi`
--

INSERT INTO `faoliyatlar_turi` (`id`, `nomi`) VALUES
(1, '1'),
(2, 'd');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1541135604),
('m130524_201442_init', 1541136818);

-- --------------------------------------------------------

--
-- Структура таблицы `talabalar_soravlari`
--

CREATE TABLE `talabalar_soravlari` (
  `id` int(11) NOT NULL,
  `turi_id` int(11) NOT NULL,
  `izoh` varchar(255) DEFAULT NULL,
  `fayl` varchar(255) NOT NULL,
  `talaba_id` int(11) NOT NULL,
  `tasdiqlash` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `talabalar_soravlari`
--

INSERT INTO `talabalar_soravlari` (`id`, `turi_id`, `izoh`, `fayl`, `talaba_id`, `tasdiqlash`) VALUES
(47, 1, 'sdsd', '1541828951.zip', 39, NULL),
(48, 1, 'sdsd', '1541828987.zip', 39, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(39, 'Nodirbek', 'acNmiHxiaibva2Aniw0JF2F557lmkymS', '$2y$13$PCFu.YhfTKHQaqYgJ79Wf.WY230yl5RCirGTVA2/NIA1AcyUEAYVa', NULL, 'nodir2@mail.ru', 10, 1541433415, 1541433415),
(40, 'test', 'YJi6OjgDsNNaEycpdGwFfrLO_V9Nr3H6', '$2y$13$uFtKvjJBG.GynkC/K66z5eCMN44gfeqP11zdT6oDOAS6HOdysrcWC', NULL, 'group.scala@mail.ru', 10, 1541491306, 1541654441),
(41, 'test2', 'ZXkUYkZRxYer2N8gvhBm2kCB5TH6VFhE', '$2y$13$KZF/RMDu/jTlq.Kfx0HG8udDI3fxg9l5LMhzRFjoqPeEip5kcPIlS', NULL, 'NodirbekRare@yandex.com', 10, 1541501049, 1541501049),
(42, 'TTTTT', '-BFXax2wowxBdsZWa0dzNA_9yLJinvLk', '$2y$13$B9u48PbgpQXF//bNN4hsJOm0KrSeKwvMK8ZCuqDTMZ7oNf9a.u8DC', NULL, 'nodir22@mail.ru', 10, 1541788413, 1541788413);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `anketa`
--
ALTER TABLE `anketa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `faoliyatlar`
--
ALTER TABLE `faoliyatlar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turi_id` (`turi_id`);

--
-- Индексы таблицы `faoliyatlar_turi`
--
ALTER TABLE `faoliyatlar_turi`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `talabalar_soravlari`
--
ALTER TABLE `talabalar_soravlari`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turi_id` (`turi_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `anketa`
--
ALTER TABLE `anketa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `faoliyatlar`
--
ALTER TABLE `faoliyatlar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `faoliyatlar_turi`
--
ALTER TABLE `faoliyatlar_turi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `talabalar_soravlari`
--
ALTER TABLE `talabalar_soravlari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `anketa`
--
ALTER TABLE `anketa`
  ADD CONSTRAINT `anketa_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `faoliyatlar`
--
ALTER TABLE `faoliyatlar`
  ADD CONSTRAINT `faoliyatlar_ibfk_1` FOREIGN KEY (`turi_id`) REFERENCES `faoliyatlar_turi` (`id`);

--
-- Ограничения внешнего ключа таблицы `talabalar_soravlari`
--
ALTER TABLE `talabalar_soravlari`
  ADD CONSTRAINT `talabalar_soravlari_ibfk_1` FOREIGN KEY (`turi_id`) REFERENCES `faoliyatlar` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
