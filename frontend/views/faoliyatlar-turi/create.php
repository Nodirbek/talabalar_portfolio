<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\FaoliyatlarTuri */

$this->title = 'Create Faoliyatlar Turi';
$this->params['breadcrumbs'][] = ['label' => 'Faoliyatlar Turis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faoliyatlar-turi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
