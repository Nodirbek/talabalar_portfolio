<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\FaoliyatlarTuriTuriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Faoliyatlar Turis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faoliyatlar-turi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Faoliyatlar Turi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomi',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
