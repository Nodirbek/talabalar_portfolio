<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\FaoliyatlarTuri */

$this->title = 'Update Faoliyatlar Turi: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Faoliyatlar Turis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faoliyatlar-turi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
