<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Kirish';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

    <!--    <p>Please fill out the following fields to login:</p>-->
<!--   --><?php //if($incorrect == false){
//       echo '<div class="alert alert-danger" role="alert">Parol yoki login noto`g`ri</div>';
//   }
   ?>
    <div class="row">
        <div class="col-md-2 col-lg-2">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/student1.png" style="width: 150px; margin-left: 40px">
        </div>
        <div class="col-md-8 col-lg-8  box box-info">
            <h3 class="text-center">KIRISH</h3>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'input-sm form-control']) ?>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <?= $form->field($model, 'password')->passwordInput(['class' => 'input-sm form-control']) ?>
                                        <?= $form->field($model, 'rememberMe')->checkbox() ?>

                                        <div style="color:#999;margin:1em 0">
                                            Agar parolingizni unutgan bo`lsangiz qayta tiklashingiz mumkin
                    <?= Html::a('parolni qayta tiklash', ['site/request-password-reset']) ?>
                                        </div>

                    <div class="form-group">
                        <?= Html::submitButton('Login', ['class' => 'button primary', 'name' => 'login-button']) ?>
                    </div>
                </div>
                <div class="col-md-2">
                </div>

            </div>


            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-md-2 col-lg-2">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/student2.png"
                 style="width: 150px; margin-left: -50px; margin-top: 30px;">

        </div>

    </div>

    </div>
