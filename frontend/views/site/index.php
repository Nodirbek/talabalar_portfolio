<?php

/* @var $this yii\web\View */
//use yii\bootstrap\ActiveForm;

$this->title = 'Talabalar';
$user_id = Yii::$app->getUser()->id;
$user_anketa = \app\models\Anketa::find()->where(['user_id' => $user_id])->one();
//$this->params['breadcrumbs'][] = $this->title;

?>
<!--<script href="--><? //=Yii::$app->request->baseUrl?><!--/js/jquery.js"></script>-->

<div class="site-index" style="margin-top: -10px">

    <div class="row body-content  shadow-sm list" style="background-color: white; ">
        <div class="col-md-12" style="margin-top: -50px;">

            <div class="col-md-8">
                <div class="row">
                    <div class="slider" style="height: 350px;">
                        <div><img src="<?= Yii::$app->request->baseUrl ?>/images/3.jpg"></div>
                        <div><img
                                src="<?= Yii::$app->request->baseUrl ?>/images/28.jpg">
                        </div>
                        <div><img src="<?= Yii::$app->request->baseUrl ?>/images/1371.jpg"></div>


                    </div>
                </div>
                <div class="col-md-3">
                    <div id="slider">
                        <ul>
                            <li>
                                <div class="slider-container">
                                    <h4>Talabalar portfoliosi</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book. It has survived not only five centuries,</p>
                                </div>
                            </li>
                            <li>
                                <div class="slider-container">
                                    <h4>Ro`yxatdan o`tish</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book. It has survived not only five centuries,</p>
                                </div>
                            </li>
                            <li>
                                <div class="slider-container">
                                    <h4>Shaxsiy kabinet</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book. It has survived not only five centuries,</p>
                                </div>
                            </li>
                            <li>
                                <div class="slider-container">
                                    <h4>Reyting ballari</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book. It has survived not only five centuries,</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hvr-overline-reveal" style="margin-top: -20px">
                <hr>
                <div class="">
                    <div class="panel-heading ">
                        <h4 style="text-align: center">MENYULAR PANELI </h4>
                    </div>
                    </div>
                <hr>
                <a href="/site/kabinet" style="color: #353535;">

                    <div class="media hvr-underline-from-center">
                        <div class="media-left">
                            <img class="media-object" src="<?= Yii::$app->request->baseUrl ?>/images/home.png""
                            alt="..." width="90" height="90">
                        </div>
                        <div class="media-body ">
                            <h4 class="media-heading ">KABINET</h4>
                            Talaba shaxsiy kabinetga kirish
                        </div>
                    </div>
                </a>
                <hr>
                <?php
                if (Yii::$app->user->isGuest){ ?>
                <a href="/site/signup" style="color: #353535;">

                    <div class="media hvr-underline-from-center">
                        <div class="media-left">
                            <img class="media-object" src="<?= Yii::$app->request->baseUrl ?>/images/registr.png"
                            alt="..." width="90" height="90">
                        </div>
                        <div class="media-body ">
                            <h4 class="media-heading ">RO'YHATDAN O'TISH</h4>
                            Talaba shaxsiy kabinetga kirish
                        </div>
                    </div>
                </a>
                <hr>
                <?php
                }
                ?>
                <a href="/anketa/index" style="color: #353535;">

                    <div class="media hvr-underline-from-center">
                        <div class="media-left">
                            <img class="media-object" src="<?= Yii::$app->request->baseUrl ?>/images/search.png"
                            alt="..." width="90" height="90">
                        </div>
                        <div class="media-body ">
                            <h4 class="media-heading ">QIDIRUV</h4>
                            Tizimdan talabani qidirish
                        </div>
                    </div>
                </a>
                <hr>
               


            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <h3>YANGILIKLAR</h3>
                <hr>

                <div class="media hvr-underline-from-center">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object" src="<?= Yii::$app->request->baseUrl ?>/images/a.jpg"
                            alt="..." width="160" height="120">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">O`quv uslubiy faoliyat</h4>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen
                        book. It has survived not only five centuries
                    </div>
                    <p class="pull-right"><i class="fa fa-eye"> 205</i> <i class="fa fa-calendar"> 2018-11-19</i></p>
                </div>
                <div class="media hvr-underline-from-center">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object" src="<?= Yii::$app->request->baseUrl ?>/images/b.jpg"
                                 alt="..." width="160" height="120">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">O`quv uslubiy faoliyat</h4>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen
                        book. It has survived not only five centuries
                    </div>
                    <p class="pull-right"><i class="fa fa-eye"> 2015</i> <i class="fa fa-calendar"> 2018-11-18</i></p>
                </div>
                <div class="media hvr-underline-from-center">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object" src="<?= Yii::$app->request->baseUrl ?>/images/background.jpg"
                                 alt="..." width="160" height="120">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">O`quv uslubiy faoliyat</h4>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen
                        book. It has survived not only five centuries
                    </div>
                    <p class="pull-right"><i class="fa fa-eye"> 2015</i> <i class="fa fa-calendar"> 2018-11-18</i></p>
                </div>


            </div>
            <div class="col-md-4">
                <div >
                    <div class="panel-heading ">
                        <h4  style="text-align: center"><i class="fa fa-book"> </i> FOYDALI RESURSLAR</h4>
                    </div>
                </div>
                <div class="list col-md-12">



                    <div class="row">
                        <div class="col-md-4"><img class="img img-thumbnail"
                                                   src="<?= Yii::$app->request->baseUrl ?>/images/str.png"
                                                   style="border-radius: 50px; width: 80px; height: 80px; "></div>
                        <div class="col-md-8"><h5 style="text-align: left; margin-top: 20px; margin-left: -25px;"><a
                                    href="http://strategy.uz?lang=uz"
                                    style="text-decoration: none;">
                                    Ривожлантиришнинг
                                    бешта устувор йўналиши бўйича Ҳаракатлар стратегияси
                                </a></h5></div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-4"><img class="img img-thumbnail"
                                                   src="<?= Yii::$app->request->baseUrl ?>/images/mygov.png"
                                                   style="border-radius: 50px; width: 80px; height: 80px; "></div>
                        <div class="col-md-8"><h5 style="text-align: left; margin-top: 20px; margin-left: -25px;"><a
                                    href="http://mygov.uz/uz"
                                    style="text-decoration: none;">Yagona
                                    interaktiv davlat xizmatlari portali</a></h5></div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-4"><img class="img img-thumbnail"
                                                   src="<?= Yii::$app->request->baseUrl ?>/images/channel256.png"
                                                   style="border-radius: 50px; width: 80px; height: 80px; "></div>
                        <div class="col-md-8"><h5 style="text-align: left; margin-top: 20px; margin-left: -25px;"><a
                                    href="http://ziyonet.uz/uz"
                                    style="text-decoration: none;">ZiyoNET
                                    jamoat axborot ta'lim tarmog’i </a></h5></div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-4"><img class="img img-thumbnail"
                                                   src="<?= Yii::$app->request->baseUrl ?>/images/lex.uz.png"
                                                   style="border-radius: 50px; width: 80px; height: 80px; "></div>
                        <div class="col-md-8"><h5 style="text-align: left; margin-top: 20px; margin-left: -25px;"><a
                                    href="http://lex.uz"
                                    style="text-decoration: none;">O`zbeksiton respublikasi qonun hijjatlari ma`lumotlar
                                    milliy bazasi</a></h5></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>