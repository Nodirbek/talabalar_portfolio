<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "RO'YHATDAN O'TISH";
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup  shadow-sm" style="background-color: white; padding: 15px">

    <div class="row ">

        <div class="col-lg-12 ">

            <div class="col-md-8">
                <h2><?= Html::encode($this->title) ?></h2>

                <p>Ro`yxatdan o`tish uchun barcha anketa ma`lumotlarini to`ldiring</p>

                <h4>Akkount ma'lumotlari</h4>
                <hr>
                <?php $form = ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' =>
                        [
                            'enctype' => 'multipart/form-data'
                        ]
                ]); ?>
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    </div>
                    <div class="col-md-6 col-lg-6">

                        <?= $form->field($model, 'email') ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <?= $form->field($model, 'password')->passwordInput() ?>

                    </div>
                    <div class="col-md-6 col-lg-6">


                    </div>
                </div>

                <hr>
            </div>
            <div class="col-md-4">
                <div class="col-md-1"></div>
                <div class=" col-md-5" style=" cursor: pointer;">
                    <img class="box box-info" src="<?= Yii::$app->request->baseUrl ?>/avatar/avatar5.png" width="200"
                         height="200">
                    <p style="text-align: center"><a class="text-center btn btn-success btn-sm" style="text-align: " id="image">     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rasm yuklash&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i
                            class="fa fa-download"></i></a></p>
                    <?= $form->field($model, 'avatar')->fileInput(["id" => "upload" ,"class"=>"hidden"]) ?>


                </div>
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-lg-12 ">
            <div class="col-md-6">
                <h4>Anketa ma'lumotlari</h4>
                <div class="row">
                    <div class="col-md-4">
                        <h5>Shaxsiy ma`lumotlar</h5>
                    </div>
                    <div class="col-md-8">
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'ism')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'familiya')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'sharif')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'tug_sana')->textInput(['type' => 'date']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'pass_ser')->dropDownList(['AA' => 'AA',
                        'AB' => 'AB','AC' => 'AC']); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'pass_raq')->textInput() ?>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-6">
                        <?= $form->field($model, 'millat')->dropDownList(['uzbek' => 'o`zbek',
                        'rus' => 'rus',
                        'qozoq' => 'qozoq',
                        'turkman' => 'turkman',
                        'tojik' => 'tojik',
                        'qoraqalpoq' => 'qoraqalpoq',
                        'karis' => 'karis']); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'jinsi')->textInput()->dropDownList(['erkak' => 'erkak',
                        'ayol' => 'ayol']); ?>
                    </div>
                </div>


                <div class="form-group">
                    <?= Html::submitButton('Ro`yxatdan o`tish', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>


            </div>
            <div class="col-md-6">
                <h4>Qo'shimcha ma`lumotlar</h4>

                <div class="row">
                    <div class="col-md-6">
                        <h5>Faoliyati haqidagi ma`lumotlar</h5>
                    </div>
                    <div class="col-md-6">
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'tugatgan_k')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'mutaxasislik')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'staj')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'sport')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'kirish_ball')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'kirish_turi')->dropDownList([
                            '1' => 'Byudjet',
                            '0' => 'Shartnoma',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
