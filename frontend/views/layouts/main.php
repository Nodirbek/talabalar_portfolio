<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="background-image: url('/frontend/web/images/background.jpg')">
<div class="">
<?php $this->beginBody() ?>

<div class="wrap container">
    <div class="navbar" style="background-color: white">
        <img src="/frontend/web/images/Career_Passport.png" style="width: 250px" alt="">
        <p class="pull-right" style="margin-top: 35px"><i class="fa fa-phone"></i> +998-97-452-92-90&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
        <a href="musobekgtd@mail.ru"><p class="pull-right" style="margin-top: 35px"><i class="fa fa-envelope"></i> musobekgtd@mail.ru&nbsp;&nbsp;&nbsp;&nbsp;</p></a>
    </div>
<p style="margin-top: -20px">
    <?php
    NavBar::begin([
        'brandLabel' => '',
//        'brandLabel' => 'LOGO',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-inverse',
        ],
    ]);
    $menuItems = [
        ['label' => 'BOSH SAHIFA', 'url' => ['/site/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => "RO'YHATDAN O'TISH", 'url' => ['/site/signup']];
        $menuItems[] = ['label' => "BIZ HAQIMIZDA", 'url' => ['/site/about']];
        $menuItems[] = ['label' => 'KIRISH', 'url' => ['/site/login']];
    } else {
        $user_id = Yii::$app->user->identity->getId();
        $user_anketa = \app\models\Anketa::find()->where(['user_id' => $user_id])->one();
        $menuItems[] = ['label' => 'KABINET', 'url' => ['/site/kabinet']];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'CHIQISH (' . Yii::$app->user->identity->username .')',
                ['class' => 'btn btn-link logout pull-rigth','style' => "color:white"]
            )
            . Html::endForm()
            . '</li>'.'<img src="'.Yii::$app->request->baseUrl.'/avatar/'.$user_anketa->avatar.'" class="" style="width: 40px; height:40px; margin-top:5px; border:2px solid white; border-radius:50px;">';

    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?></p>

    <div class="container" style="background-color: white; margin-top: -20px">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer container">
    <div class="container">
        <p class="pull-left">&copy; Talabalar reytingi <?= date('Y') ?></p>

<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
    </div>
</footer>

<?php $this->endBody() ?>
<script >
    $(document).ready(function () {
        $("#image").click(function () {
            console.log('as');
            $("#upload").trigger("click");
        });

        $(':file').on('fileselect', function(event, numFiles, label) {
            console.log(numFiles);
            console.log(label);
            $("#filePath").val(label);
        });
        $('#upload').on('fileselect', function(event, numFiles, label) {
            console.log($(this).val());
           $('#submit').removeClass('hidden');
        });
        $('#submit').on('click', function() {
           $('#submit').trigger('click');
        });
        $(document).on('change', ':file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });
        
    })
        
</script>
</body>
</html>
<?php $this->endPage() ?>
