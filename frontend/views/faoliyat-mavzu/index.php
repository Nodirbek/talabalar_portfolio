<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\FaoliyatMavzuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Faoliyat Mavzus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faoliyat-mavzu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Faoliyat Mavzu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomi',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
