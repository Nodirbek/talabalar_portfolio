<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Faoliyatlar */
/* @var $form yii\widgets\ActiveForm */
$product = \frontend\models\FaoliyatMavzu::find()->all();
$data = \yii\helpers\ArrayHelper::map($product,'id' ,'nomi');

?>

<div class="faoliyatlar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'turi_id')->textInput() ?>

<!--    --><?//= $form->field($model, 'nomi')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'nomi')->dropDownList($data) ?>

    <?= $form->field($model, 'izoh')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ball')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
