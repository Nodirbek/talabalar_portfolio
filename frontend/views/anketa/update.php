<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Anketa */

$this->title = 'Shaxsiy ma`lumotlarni tahrirlash: ' . $model->ism . $model->familiya;
$this->params['breadcrumbs'][] = ['label' => 'Kabinet', 'url' => ['/site/kabinet']];
$this->params['breadcrumbs'][] = 'Shaxsiy ma`lumotlarni tahrirlash';
?>
<div class="anketa-update ">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
