<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Anketa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anketa-form shadow-sm list" style="background-color: white; padding: 15px"">

<?php $form = ActiveForm::begin(); ?>

<!--    --><? //= $form->field($model, 'user_id')->textInput() ?>

<?= $form->field($model, 'familiya')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'ism')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'sharif')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'millat')->dropDownList([
    'uzbek' => 'o`zbek',
    'rus' => 'rus',
    'qozoq' => 'qozoq',
    'turkman' => 'turkman',
    'tojik' => 'tojik',
    'qoraqalpoq' => 'qoraqalpoq',
    'karis' => 'karis'
]); ?>
<?= $form->field($model, 'pass_ser')->dropDownList([
    'AA' => 'AA',
    'AB' => 'AB'
]); ?>
<?= $form->field($model, 'pass_raq')->textInput() ?>

<?= $form->field($model, 'tugatgan_k')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'tugatgan_yili')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'mutaxasislik')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'staj')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'sport')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'kirish_ball')->textInput() ?>

<?= $form->field($model, 'kirish_turi')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'avatar')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'tug_sana')->textInput(['type' => 'date']) ?>

<? //= $form->field($model, 'jinsi')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'jinsi')->textInput()->dropDownList([
    'erkak' => 'erkak',
    'ayol' => 'ayol'
]); ?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
