<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AnketaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qidiruv';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anketa-index">

    <h1><?= Html::encode($this->title) ?> <i class="fa fa-search"></i></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Anketa', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'user_id',
            'familiya',
            'ism',
            'sharif',
            'millat',
//            'pass_ser',
//            'pass_raq',
//            'tugatgan_k',
//            'tugatgan_yili',
            'mutaxasislik',
//            'staj',
//            'sport',
//            'kirish_ball',
//            'kirish_turi',
            'avatar',
//            'tug_sana',
            'jinsi',
            [
                'attribute' => 'avatar',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<a href="/site/info?id_user='.$data->id.'" class="btn btn-primary btn-xs fa fa-eye"> Info</a>';
                },

            ],
            [
                'attribute' => 'avatar',
                'format' => 'raw',
                'value' => function ($data) {
                 return '<img src="'.Yii::$app->request->baseUrl.'/avatar/'.$data->avatar.'" width="100px" height="120px">';
                },

            ],


//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
