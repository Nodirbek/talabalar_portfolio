<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\AnketaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anketa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'familiya') ?>

    <?= $form->field($model, 'ism') ?>

    <?= $form->field($model, 'sharif') ?>

    <?php // echo $form->field($model, 'millat') ?>

    <?php // echo $form->field($model, 'pass_ser') ?>

    <?php // echo $form->field($model, 'pass_raq') ?>

    <?php // echo $form->field($model, 'tugatgan_k') ?>

    <?php // echo $form->field($model, 'tugatgan_yili') ?>

    <?php // echo $form->field($model, 'mutaxasislik') ?>

    <?php // echo $form->field($model, 'staj') ?>

    <?php // echo $form->field($model, 'sport') ?>

    <?php // echo $form->field($model, 'kirish_ball') ?>

    <?php // echo $form->field($model, 'kirish_turi') ?>

    <?php // echo $form->field($model, 'avatar') ?>

    <?php // echo $form->field($model, 'tug_sana') ?>

    <?php // echo $form->field($model, 'jinsi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
