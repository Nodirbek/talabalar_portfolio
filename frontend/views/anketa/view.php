<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Anketa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kabinet', 'url' => ['/site/kabinet']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anketa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'familiya',
            'ism',
            'sharif',
            'millat',
            'pass_ser',
            'pass_raq',
            'tugatgan_k',
            'tugatgan_yili',
            'mutaxasislik',
            'staj',
            'sport',
            'kirish_ball',
            'kirish_turi',
            'avatar',
            'tug_sana',
            'jinsi',
        ],
    ]) ?>

</div>
