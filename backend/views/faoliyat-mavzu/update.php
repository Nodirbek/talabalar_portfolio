<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FaoliyatMavzu */

$this->title = '';
?>
<div class="panel panel-default" style="padding: 20px 20px 20px 20px">

    <h3 style="text-align: center">Фаолият мезонлар</h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
