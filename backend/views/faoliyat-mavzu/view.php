<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\FaoliyatMavzu */

$this->title = '';
?>
<div class="faoliyat-mavzu-view">
    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <?= Html::a('янгилаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'nomi',
            [
                'attribute' => 'Faoliyat turi',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = \frontend\models\FaoliyatlarTuri::find()->where(['id' => $data->id_tur])->one();
                    $name = $modelUser->nomi;
                    return $name;
                },
            ],
        ],
    ]) ?>

</div>
