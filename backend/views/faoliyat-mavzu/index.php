<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FaoliyatMavzuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-default" style="padding: 20px 20px  20px 20px">

    <h2 style="text-align: center">Фаолият мезонлар</h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Faoliyat turi',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = \frontend\models\FaoliyatlarTuri::find()->where(['id' => $data->id_tur])->one();
                    $name = $modelUser->nomi;
                    return $name;
                },
            ],
            [
                'attribute' => 'Nomi',
                'format' => 'raw',
                'value' => function ($data) {
                    $name = mb_substr($data->nomi,0,60)."...";
                    return $name;
                },
            ],



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
