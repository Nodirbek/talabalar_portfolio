<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FaoliyatMavzu */
/* @var $form yii\widgets\ActiveForm */

$tur = \backend\models\FaoliyatlarTuri::find()->all();
$data = \yii\helpers\ArrayHelper::map($tur,'id','nomi');
?>

<div class="faoliyat-mavzu-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row ">
        <div class="col-md-6">
            <?= $form->field($model, 'nomi')->textInput(['' => true]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'id_tur')->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\FaoliyatlarTuri::find()->all(), 'id', 'nomi')) ?>

        </div>



    <div class="form-group" style="padding: 15px 15px 15px 15px">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
