<?php

/* @var $this yii\web\View */

$this->title = '';
?>

<h3 style="text-align: center; color:rgba(33, 102, 254, 0.61);"><?=$name?></h3>
<br>
<table class="table" id="hoverTable">
    <thead style="background: #8f74ff">
    <tr  style="background-color: rgba(33, 102, 254, 0.61)">
        <th style="color: white">#</th>
        <th nowrap style="color: white">Талаба</th>
        <th nowrap style="color: white">Фаолиеат тури</th>
        <th nowrap style="color: white">Фаолиеат</th>
        <th nowrap style="color: white">Изох</th>
        <th nowrap style="color: white">Қайллар</th>
        <th nowrap style="color: white">Холат</th>
        <th style="color: white"></th>
        <th style="color: white"></th>
    </tr>
    </thead>
    <?php
    $i=0;
    foreach ($model as $item) { $i++
    ?>
    <tbody>
    <tr>
        <th scope="row"><?= $i; ?></th>
        <td><?php
            echo $item->fname." ";
            echo $item->name." ";
            echo $item->lname?></td>
        <td><?=$item->faol_name?></td>
        <td><?=$item->fcommit?></td>
        <td style="width: 300px"><?=$item->commit?></td>
        <td><a download href="/frontend/web/file/<?=$item->file?>"> File<?=$item->file?></a></td>
        <td>
            <?php
            if($item->tasdiq == 0){
                echo " <img src='/backend/web/images/677e7103-550582.png' style='width:40px' alt=\"\">";
            }
            else{
                echo " <img src='/backend/web/images/email-marketing.png' style='width:40px' alt=\"\">";
            }

            ?>
            <img src="" alt="">
        </td>
        <td>

            <?php \yii\widgets\ActiveForm::begin()?>
            <?php
            if($item->tasdiq == 0){
                echo " <input type='text' value=' $item->ids' hidden name='true'>";
                echo " <button class='btn btn-info' style='margin-top: 4px'><i class=\"fa fa-check\"></i> Тасдиқлаш</button>";
            }
            if($item->tasdiq == 1){

                echo " <input type='text' hidden value=' $item->ids' name='false'>";
                echo " <button class='btn btn-danger' style='margin-top: 4px'><i class=\"fa fa-close\"></i> Бекор қилиш</button>";
            }
            if($item->tasdiq == 3){
                echo " <input type='text' value=' $item->ids' hidden name='true'>";
                echo " <button class='btn btn-info' style='margin-top: 4px'><i class=\"fa fa-check\"></i> Қабул қилиш</button>";
            }

            ?>

            <?php \yii\widgets\ActiveForm::end()?>
        </td>
        <?php \yii\widgets\ActiveForm::begin()?>
        <?php

        ?>
        <input type='text' hidden value='<?= $item->ids ?>' name='false1'>
        <td><button class='btn btn-danger' style='margin-top: 4px'><i class="fa fa-close"></i> Қабул қилинмади</button></td>
        <?php \yii\widgets\ActiveForm::end()?>
    </tr>

    </tbody>
    <?php }?>
</table>

<style style="text/css">
    #hoverTable{
    width:100%;

    }
    #hoverTable td{
    padding:2px;
    }
    /* Define the default color for all the table rows */
    #hoverTable tr{
    background: white;
    }
    /* Define the hover highlight color for the table row */
    #hoverTable tr:hover {
    background-color: #ccedff;
    }

</style>