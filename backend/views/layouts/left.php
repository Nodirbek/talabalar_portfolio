<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center" >
                <img src="<?=Yii::$app->request->baseUrl?>/avatar.png" style="width: 50%" class="img-thumbnail" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p style="text-align: center; margin-top: 10px">

                    </p>

            </div>
        </div>

        <!-- search form -->
        <!-- search form -->
        <hr>
        <!-- /.search form -->

        <?php


        echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => "Бош сахифа", 'icon' => 'home', 'url' => ['/site/index']],

                    ['label' => "Фаолият мезонлар", 'icon' => 'list-alt', 'url' => ['faoliyat-mavzu/index']],
                    ['label' => "Мезон изохлари ва баллари", 'icon' => 'plus', 'url' => ['faoliyatlar/index']],
                    ['label' => "Фаолият турлари", 'icon' => 'tag', 'url' => ['faoliyatlar-turi/index']],


                ],
            ]
        );
        ?>






    </section>

</aside>
