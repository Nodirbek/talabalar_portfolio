<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FaoliyatlarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-default" style="padding: 20px 20px 20px 20px">

    <h2 style="text-align: center">Фаолият мезони изохлари ва баллари</h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Faoliyat nomi',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = \frontend\models\FaoliyatMavzu::find()->where(['id' => $data->nomi])->one();
                    $name = mb_substr($modelUser->nomi,0,60);
                    return $name;
                },
            ],
            [
                'attribute' => 'Izoh',
                'format' => 'raw',
                'value' => function ($data) {
                    $name = mb_substr($data->izoh,0,60)."...";
                    return $name;
                },
            ],
            'ball',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
