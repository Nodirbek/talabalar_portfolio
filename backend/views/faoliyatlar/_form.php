<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Faoliyatlar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row" style="padding: 40px 40px 40px 40px">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-4">
        <?= $form->field($model, 'nomi')->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\FaoliyatMavzu::find()->all(), 'id', 'nomi')) ?>

    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'izoh')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'ball')->textInput() ?>

    </div>



    <div class="form-group" style="padding: 15px 15px 15px 15px">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
