<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Faoliyatlar */

$this->title = 'Update Faoliyatlar: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Faoliyatlars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faoliyatlar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
