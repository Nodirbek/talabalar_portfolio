<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Faoliyatlar */

$this->title = '';
?>
<div class="panel panel-default">

    <h2 style="text-align: center">Фаолият мезони изохлари ва баллари</h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
