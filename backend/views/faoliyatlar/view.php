<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Faoliyatlar */

$this->title = "";

?>
<div class="panel panel-default" style="padding: 10px 10px 10px 10px">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('янгилаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nomi',
            'izoh',
            'ball',
        ],
    ]) ?>

</div>
