<?php
namespace backend\controllers;

use backend\models\TalabalarSoravlari;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout','oquv', 'index','ilmiy','tashkiliy','manaviy'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $tur = \frontend\models\FaoliyatlarTuri::find()->all();
        $model = \frontend\models\FaoliyatlarTuri::find()
            ->select('talabalar_soravlari.izoh as commit,count(faoliyatlar_turi.id) as fcount,talabalar_soravlari.id as ids,talabalar_soravlari.fayl as file,faoliyatlar.izoh as fcommit,talabalar_soravlari.tasdiqlash as tasdiq,faoliyatlar_turi.id as id,faoliyatlar_turi.nomi as faol_name')
            -> innerJoin('faoliyat_mavzu','faoliyat_mavzu.id_tur = faoliyatlar_turi.id ')
            -> innerJoin('faoliyatlar','faoliyatlar.nomi = faoliyat_mavzu.id')
            -> innerJoin('talabalar_soravlari','faoliyatlar.id = talabalar_soravlari.turi_id and talabalar_soravlari.tasdiqlash=0')
            ->groupBy('faoliyatlar_turi.id')->asArray()->all();

        return $this->render('index',[

            'model'=>$model,

            'tur'=>$tur,
        ]);

    }

    public function actionOquv($id)
    {
        $name = "Ўқув услубий ишлар бўйича";

        TalabalarSoravlari::updateAll(['tasdiqlash'=>1],['id'=>Yii::$app->request->post('true')]);
        TalabalarSoravlari::updateAll(['tasdiqlash'=>0],['id'=>Yii::$app->request->post('false')]);
        TalabalarSoravlari::updateAll(['tasdiqlash'=>3],['id'=>Yii::$app->request->post('false1')]);
        $model = \backend\models\TalabalarSoravlari::find()->where(['faoliyatlar_turi.id'=>$id])
            ->select('talabalar_soravlari.izoh as commit,talabalar_soravlari.id as ids,anketa.ism as name,anketa.familiya as fname,anketa.sharif as lname,  talabalar_soravlari.fayl as file,faoliyatlar.izoh as fcommit,talabalar_soravlari.tasdiqlash as tasdiq,faoliyatlar_turi.nomi as faol_name')
            -> innerJoin('faoliyatlar','faoliyatlar.id = talabalar_soravlari.turi_id')
            -> innerJoin('faoliyat_mavzu','faoliyatlar.nomi = faoliyat_mavzu.id')
            -> innerJoin('user','user.id = talabalar_soravlari.talaba_id')
            -> innerJoin('anketa','anketa.user_id = user.id')
            -> innerJoin('faoliyatlar_turi','faoliyat_mavzu.id_tur = faoliyatlar_turi.id ')->
            orderBy(['talabalar_soravlari.id'=>SORT_DESC])->all();
//        $dataProvider = new ActiveDataProvider([
//
//            'query'=>$model
//        ]);

        return $this->render('oquv',[

            'model'=>$model,
//            "name"=>$name
        ]);
    }
    public function actionIlmiy()
    {
        $name ="Илмий ишлар бўйича";

        TalabalarSoravlari::updateAll(['tasdiqlash'=>1],['id'=>Yii::$app->request->post('true')]);
        TalabalarSoravlari::updateAll(['tasdiqlash'=>0],['id'=>Yii::$app->request->post('false')]);
        TalabalarSoravlari::updateAll(['tasdiqlash'=>3],['id'=>Yii::$app->request->post('false1')]);
        $model = TalabalarSoravlari::find()
            ->select('talabalar_soravlari.izoh as commit,talabalar_soravlari.id as ids,talabalar_soravlari.fayl as file,faoliyatlar.izoh as fcommit,talabalar_soravlari.tasdiqlash as tasdiq,faoliyatlar_turi.nomi as faol_name,anketa.ism as name,anketa.familiya as fname,anketa.sharif as lname,')
            -> innerJoin('faoliyatlar','faoliyatlar.id = talabalar_soravlari.turi_id')
            -> innerJoin('user','user.id = talabalar_soravlari.talaba_id')
            -> innerJoin('anketa','anketa.user_id = user.id')
            -> innerJoin('faoliyatlar_turi','faoliyatlar_turi.id = faoliyatlar.nomi and faoliyatlar_turi.id = 1')
            ->orderBy(['talabalar_soravlari.id'=>SORT_DESC])
            ->all();
        return $this->render('oquv',[

            'model'=>$model,
            "name"=>$name
        ]);
    }
    public function actionManaviy()
    {
        $name ="Маънавий-маърифий ишлар бўйича";

        TalabalarSoravlari::updateAll(['tasdiqlash'=>1],['id'=>Yii::$app->request->post('true')]);
        TalabalarSoravlari::updateAll(['tasdiqlash'=>0],['id'=>Yii::$app->request->post('false')]);
        TalabalarSoravlari::updateAll(['tasdiqlash'=>3],['id'=>Yii::$app->request->post('false1')]);
        $model = TalabalarSoravlari::find()
            ->select('talabalar_soravlari.izoh as commit,talabalar_soravlari.id as ids,talabalar_soravlari.fayl as file,faoliyatlar.izoh as fcommit,talabalar_soravlari.tasdiqlash as tasdiq,faoliyatlar_turi.nomi as faol_name,anketa.ism as name,anketa.familiya as fname,anketa.sharif as lname,')
            -> innerJoin('faoliyatlar','faoliyatlar.id = talabalar_soravlari.turi_id')
            -> innerJoin('user','user.id = talabalar_soravlari.talaba_id')
            -> innerJoin('anketa','anketa.user_id = user.id')
            -> innerJoin('faoliyatlar_turi','faoliyatlar_turi.id = faoliyatlar.nomi and faoliyatlar_turi.id = 3')
            ->orderBy(['talabalar_soravlari.id'=>SORT_DESC])
            ->all();
        return $this->render('oquv',[

            'model'=>$model,
            "name"=>$name
        ]);
    }
    public function actionTashkiliy()
    {
        $name ="Ташкилий-услубий ишлар бўйича";

        TalabalarSoravlari::updateAll(['tasdiqlash'=>1],['id'=>Yii::$app->request->post('true')]);
        TalabalarSoravlari::updateAll(['tasdiqlash'=>0],['id'=>Yii::$app->request->post('false')]);
        TalabalarSoravlari::updateAll(['tasdiqlash'=>3],['id'=>Yii::$app->request->post('false1')]);
        $model = TalabalarSoravlari::find()
            ->select('talabalar_soravlari.izoh as commit,talabalar_soravlari.id as ids,talabalar_soravlari.fayl as file,faoliyatlar.izoh as fcommit,talabalar_soravlari.tasdiqlash as tasdiq,faoliyatlar_turi.nomi as faol_name,anketa.ism as name,anketa.familiya as fname,anketa.sharif as lname,')
            -> innerJoin('faoliyatlar','faoliyatlar.id = talabalar_soravlari.turi_id')
            -> innerJoin('user','user.id = talabalar_soravlari.talaba_id')
            -> innerJoin('anketa','anketa.user_id = user.id')
            -> innerJoin('faoliyatlar_turi','faoliyatlar_turi.id = faoliyatlar.nomi and faoliyatlar_turi.id = 4')
            ->orderBy(['talabalar_soravlari.id'=>SORT_DESC])
            ->all();
        return $this->render('oquv',[

            'model'=>$model,
            "name"=>$name
        ]);
    }
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
