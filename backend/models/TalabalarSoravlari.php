<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "talabalar_soravlari".
 *
 * @property int $id
 * @property int $turi_id
 * @property string $izoh
 * @property string $fayl
 * @property int $talaba_id
 * @property int $tasdiqlash
 *
 * @property Faoliyatlar $turi
 */
class TalabalarSoravlari extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'talabalar_soravlari';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turi_id', 'fayl', 'talaba_id'], 'required'],
            [['turi_id', 'talaba_id', 'tasdiqlash'], 'integer'],
            [['izoh', 'fayl'], 'string', 'max' => 255],
            [['turi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faoliyatlar::className(), 'targetAttribute' => ['turi_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public $file;
    public $faol_name;
    public $name;
    public $fname;
    public $tasdiq;
    public $commit;
    public $fcommit;
    public $ids;
    public $lname;
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'turi_id' => 'Turi ID',
            'izoh' => 'Izoh',
            'fayl' => 'Fayl',
            'talaba_id' => 'Talaba ID',
            'tasdiqlash' => 'Tasdiqlash',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTuri()
    {
        return $this->hasOne(Faoliyatlar::className(), ['id' => 'turi_id']);
    }
}
